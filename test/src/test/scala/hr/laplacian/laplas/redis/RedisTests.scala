package hr.laplacian.laplas.redis

import com.redis.RedisClientPool
import testz.{Harness, assert}

object RedisTests
{
  private def initRedis: (RedisServiceImpl, RedisQueueService) = {
    // docker run -p 6400:6379 redis
    val r = new RedisClientPool("localhost", 6400)
    r.withClient(_.flushdb) // <- using docker redis because of this!
    (new RedisServiceImpl(r), new RedisQueueServiceImpl(r))
  }

  def sanityCheck[T](harness: Harness[T]): T = {
    import harness._
    val (redisService, _) = initRedis

    section("sanity check")(
      test("get result from empty redis dataset"){ () =>
        assert(redisService.get("test").isEmpty)
      },
      test("set value 12345 to key test"){ () =>
        assert(redisService.set("test", "12345"))
      },
      test("verify if there is result for key test"){ () =>
        assert(redisService.get("test").isDefined)
      },
      test("pipeline operations"){ () =>
        val pipeline = redisService
          .clients
          .withClient(_.pipeline{ p =>
            p.get("key")
            p.set("key", "12345")
            p.get("key")
          })
        assert(pipeline.contains(List(None, true, Some("12345"))))
      }
    )
  }

  def customOperations[T](harness: Harness[T]): T = {
    import harness._

    val (redisService, redisQueueService) = initRedis

    section("custom functions")(
      test("zAddByScoreAtomically"){ () =>
        val resultSet = List(
          redisService.zAddByScoreAtomically("score", 10, "1"),
          redisService.zAddByScoreAtomically("score", 11, "1"),
          redisService.zAddByScoreAtomically("score", 11, "1"),
          redisService.zAddByScoreAtomically("score", 20, "2"),
          redisService.zAddByScoreAtomically("score", 21, "2"),
          redisService.zAddByScoreAtomically("score", 22, "2"),
          redisService.zAddByScoreAtomically("score", 22, "2")
        )
        assert(resultSet equals List(1, 1, 0, 1, 1, 1, 0))
      },
      test("getRateLimitToken"){ () =>
        import scala.concurrent.duration._

        val resultSet = List(
          redisService.getRateLimitToken("ratetoken1", 2, 1.minute),
          redisService.getRateLimitToken("ratetoken1", 2, 1.minute),
          redisService.getRateLimitToken("ratetoken1", 2, 1.minute),
          redisService.getRateLimitToken("ratetoken2", 1, 1.nano),
          redisService.getRateLimitToken("ratetoken2", 1, 1.nano)
        )
        val bucketSize1 = redisService.getCurrentBucketSize("ratetoken1", 1.minute)
        val bucketSize2 = redisService.getCurrentBucketSize("ratetoken2", 1.nano)

        val res1 = bucketSize1 == 2 && bucketSize2 == 0

        val res2 = resultSet match {
          case Some(firstToken) :: Some(secondToken) :: None :: Some(_) :: Some(lastToken) :: Nil =>
            redisService.returnRateLimitToken("ratetoken1", secondToken)
            val bucketSize1 = redisService.getCurrentBucketSize("ratetoken1", 1.minute)
            val firstTokenIsValid = redisService.validateRateLimitToken("ratetoken1", firstToken, 1.minute)
            val secondTokenIsValid = redisService.validateRateLimitToken("ratetoken1", secondToken, 1.minute)
            val lastTokenIsValid = redisService.validateRateLimitToken("ratetoken2", lastToken, 1.nano)
            firstTokenIsValid && !secondTokenIsValid && !lastTokenIsValid && bucketSize1 == 1
          case _ => false
        }
        assert(res1 && res2)
      },
      test("ticketQueueSystem"){ () =>
        import scala.concurrent.duration._

        val queueKey = "the_test_queue"
        val intervalDuration = 500.millis
        val intervalDiff = 150.millis

        val ticketNumbers = List(
          redisQueueService.getQueueTicket(queueKey, intervalDuration),
          redisQueueService.getQueueTicket(queueKey, intervalDuration),
          redisQueueService.getQueueTicket(queueKey, intervalDuration),
          redisQueueService.getQueueTicket(queueKey, intervalDuration),
        )

        val queueSize1 = redisQueueService.getCurrentQueueSize(queueKey, intervalDuration)

        Thread.sleep(intervalDuration.minus(intervalDiff).toMillis)

        ticketNumbers.tail.map(ticketNumber =>
          redisQueueService.holdPlaceInQueue(queueKey, ticketNumber, intervalDuration),
        )

        Thread.sleep(intervalDuration.minus(intervalDiff).toMillis)

        val resultSet = ticketNumbers.map(ticketNumber =>
          redisQueueService.holdPlaceInQueue(queueKey, ticketNumber, intervalDuration)
        )

        val queueSize2 = redisQueueService.getCurrentQueueSize(queueKey, intervalDuration)

        Thread.sleep(intervalDuration.minus(intervalDiff).toMillis)

        val resultSet2 = ticketNumbers.map(ticketNumber =>
          redisQueueService.tryToUseQueueTicket(queueKey, ticketNumber, intervalDuration)
        )

        val queueSize3 = redisQueueService.getCurrentQueueSize(queueKey, intervalDuration)

        Thread.sleep(intervalDuration.minus(intervalDiff).toMillis)

        val resultSet3 = ticketNumbers.map(ticketNumber =>
          redisQueueService.tryToUseQueueTicket(queueKey, ticketNumber, intervalDuration)
        )

        val res = resultSet match {
          case None :: Some(0) :: Some(1) :: Some(2) :: Nil => true
          case _ => false
        }

        val res2 = resultSet2 match {
          case None :: Some(0) :: Some(0) :: Some(0) :: Nil => true
          case _ => false
        }

        val res3 = resultSet3 match {
          case None :: None :: None :: None :: Nil => true
          case _ => false
        }

        val res4 = queueSize1 == 4 && queueSize2 == 3 && queueSize3 == 0

        assert(res && res2 && res3 && res4)
      }
    )
  }
}
