Laplas Redis
=================================
 - Synchronous redis service wrapping common functionality and extending it with several new ones

 - `docker run -p 6400:6379 redis`
 - `sbt "project redis-tests" test:run` or `sbt` `redis-tests/test:run`
 