package hr.laplacian.laplas.redis

import java.util.concurrent.TimeUnit
import java.util.{Calendar, UUID}

import com.redis._
import javax.inject.Inject

import scala.concurrent.duration.{Duration, FiniteDuration}

class RedisServiceImpl @Inject()
(
  val clients: RedisClientPool
) extends RedisService
{
  override def set(key : String, value : String) : Boolean = {
    clients.withClient {
      redis => {
        redis.set(key, value)
      }
    }
  }

  override def setEx(key : String, value : String, ttl : FiniteDuration) : Boolean = {
    clients.withClient {
      redis => {
        redis.setex(key, ttl.toSeconds, value)
      }
    }
  }

  override def get(key : String) : Option[String] = {
    clients.withClient {
      redis => {
        redis.get(key)
      }
    }
  }

  override def del(key: String): Long = {
    clients.withClient {
      redis => {
        redis.del(key).getOrElse(0)
      }
    }
  }

  override def expire(key: String, ttl: FiniteDuration): Boolean = {
    clients.withClient {
      redis => {
        redis.expire(key, ttl.toSeconds.toInt)
      }
    }
  }

  override def getTTL(key: String): Option[Duration] = {
    clients.withClient {
      redis => {
        redis.pttl(key).flatMap {
          case RedisServiceImpl.KEY_DOES_NOT_EXIST => None
          case RedisServiceImpl.NO_TTL             => Some(Duration.Inf)
          case  0                                  => Some(Duration.Zero)
          case ttl if ttl > 0                      => Some(Duration(ttl, TimeUnit.SECONDS))
          case _                                   => None
        }
      }
    }
  }

  override def zRem(key : String, member : String) : Boolean = {
    clients.withClient {
      redis => {
        redis.zrem(key, member).fold(false)(_ => true)
      }
    }
  }

  override def zCard(key: String): Long = {
    clients.withClient {
      redis => {
        redis.zcard(key).getOrElse(0)
      }
    }
  }

  override def zScore(key: String, member: String): Option[Double] = {
    clients.withClient {
      redis => {
        redis.zscore(key, member)
      }
    }
  }

  override def zRevRank(key: String, member: String): Option[Long] = {
    clients.withClient {
      redis => {
        redis.zrank(key, member, reverse = true)
      }
    }
  }

  override def zAdd(key: String, score: Double, member: String): Long = {
    clients.withClient {
      redis => {
        redis.zadd(key, score, member).getOrElse(0)
      }
    }
  }

  override def zIncrBy(key: String, score: Double, member: String): Double = {
    clients.withClient {
      redis => {
        redis.zincrby(key, score, member).getOrElse(0)
      }
    }
  }

  override def zRank(key: String, member: String): Option[Long] = {
    clients.withClient {
      redis => {
        redis.zrank(key, member)
      }
    }
  }

  override def zRange(key: String, start: Long = RedisServiceImpl.FIRST, end: Long = RedisServiceImpl.LAST): List[String] = {
    clients.withClient {
      redis => {
        redis.zrange(key, start.toInt, end.toInt)
          .fold(List.empty[String])(_.toList)
      }
    }
  }

  override def zRevRangeWithScore(key: String, start: Long = RedisServiceImpl.FIRST, end: Long = RedisServiceImpl.LAST): List[(String, Double)] = {
    clients.withClient {
      redis => {
        redis.zrangeWithScore(key, start.toInt, end.toInt, com.redis.RedisClient.DESC)
          .fold(List.empty[(String, Double)])(_.toList)
      }
    }
  }

  override def zAddByScoreAtomically(key: String, score: Double, member: String): Long = {
    clients.withClient {
      redis => {
        redis.pipeline { p =>
          p.zremrangebyscore(key, score, score)
          p.zadd(key, score, member)
        }
          .flatMap(_.head.asInstanceOf[Option[Long]])
          .fold(RedisServiceImpl.NEW_ELEMENT_ADDED) {
            case RedisServiceImpl.NO_ELEMENTS_REMOVED => RedisServiceImpl.NEW_ELEMENT_ADDED
            case _ => RedisServiceImpl.ELEMENT_UPDATED
          }
      }
    }
  }

  override def getRateLimitToken(key: String, limit : Long, interval : Duration) : Option[String] = {
    val token = UUID.randomUUID().toString
    val timestamp = Calendar.getInstance().getTimeInMillis
    clients.withClient {
      redis => {
        redis.pipeline { p =>
          p.zremrangebyscore(key, RedisServiceImpl.FIRST, timestamp - interval.toMillis)
          p.zadd(key, timestamp, token)
          p.zcard(key) // index 2
          p.expire(key, interval.toSeconds.toInt)
        }
          .flatMap(_(2).asInstanceOf[Option[Long]]) // take at index 2
          .flatMap(card =>
          if(card > limit) {
            redis.zrem(key, token)
            None
          } else Some(token)
        )
      }
    }
  }

  override def returnRateLimitToken(key : String, token : String) : Boolean = zRem(key, token)

  override def validateRateLimitToken(key: String, token: String, interval: Duration): Boolean = {
    val timestamp = Calendar.getInstance().getTimeInMillis
    clients.withClient {
      redis => {
        redis.pipeline { p =>
          p.zremrangebyscore(key, RedisServiceImpl.FIRST, timestamp - interval.toMillis)
          p.zscore(key, token)
        }
          .flatMap(_.last.asInstanceOf[Option[Double]])
          .isDefined
      }
    }
  }

  override def getCurrentBucketSize(key: String, interval: Duration): Long = {
    val timestamp = Calendar.getInstance().getTimeInMillis
    clients.withClient {
      redis => {
        redis.pipeline { p =>
          p.zremrangebyscore(key, RedisServiceImpl.FIRST, timestamp - interval.toMillis)
          p.zcard(key)
        }
          .flatMap(_.last.asInstanceOf[Option[Long]])
          .getOrElse(RedisServiceImpl.NO_ELEMENTS)
      }
    }
  }
}

object RedisServiceImpl
{
  private val FIRST = 0
  private val LAST = -1

  private val NO_ELEMENTS_REMOVED = 0

  private val NO_ELEMENTS = 0

  private val NEW_ELEMENT_ADDED = 1L
  private val ELEMENT_UPDATED = 0L

  private val KEY_DOES_NOT_EXIST = -2
  private val NO_TTL = -1
}
