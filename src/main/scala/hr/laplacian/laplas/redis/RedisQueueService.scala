package hr.laplacian.laplas.redis

import scala.concurrent.duration.FiniteDuration

trait RedisQueueService {
  def getQueueTicket(key : String, queryInterval : FiniteDuration) : Long
  def holdPlaceInQueue(key : String, ticketNumber : Long, queryInterval : FiniteDuration) : Option[Long]
  def tryToUseQueueTicket(key : String, ticketNumber : Long, queryInterval : FiniteDuration) : Option[Long]
  def getCurrentQueueSize(key : String, queryInterval : FiniteDuration) : Long
}
