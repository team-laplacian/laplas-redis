package hr.laplacian.laplas.redis

import java.util.Calendar

import com.redis.{RedisClient, RedisClientPool}
import javax.inject.Inject

import scala.concurrent.duration._

class RedisQueueServiceImpl @Inject()
(
  val clients: RedisClientPool
) extends RedisQueueService
{
  override def getQueueTicket(key: String, queryInterval: FiniteDuration): Long = {
    val ticketNumber = Calendar.getInstance().getTimeInMillis
    val lineKey = s"${key}_line"
    val queryKey = s"${key}_query"
    clients.withClient {
      redis => {
        redis.pipeline { p =>
          p.zadd(lineKey, ticketNumber, ticketNumber)
          p.zadd(queryKey, ticketNumber, ticketNumber)
          p.expire(lineKey, queryInterval.plus(1.minute).toSeconds.toInt)
          p.expire(queryKey, queryInterval.plus(1.minute).toSeconds.toInt)
        }
      }
    }
    ticketNumber
  }

  override def holdPlaceInQueue(key: String, ticketNumber: Long, queryInterval: FiniteDuration): Option[Long] = refreshQueueTicket(key, ticketNumber, popIfFirst = false, queryInterval)

  override def tryToUseQueueTicket(key: String, ticketNumber: Long, queryInterval: FiniteDuration): Option[Long] = refreshQueueTicket(key, ticketNumber, popIfFirst = true, queryInterval)

  private def refreshQueueTicket(key: String, ticketNumber: Long, popIfFirst : Boolean, queryInterval: FiniteDuration): Option[Long] = {
    val timestamp = Calendar.getInstance().getTimeInMillis
    val lineKey = s"${key}_line"
    val queryKey = s"${key}_query"

    clients.withClient {
      redis => {
        redis
          .pipeline { p =>
            p.zremrangebyscore(queryKey, RedisQueueServiceImpl.FIRST, timestamp - queryInterval.toMillis)
            p.zinterstore(lineKey, List(lineKey, queryKey), RedisClient.MIN)
            p.zrank(lineKey, ticketNumber) // index 2
            p.expire(lineKey, queryInterval.plus(1.minute).toSeconds.toInt)
            p.expire(queryKey, queryInterval.plus(1.minute).toSeconds.toInt)
          }
          .flatMap(_(2).asInstanceOf[Option[Long]]) // take at index 2
          .flatMap{ rank =>
            if(rank == RedisQueueServiceImpl.FIRST && popIfFirst) {
              redis.zrem(lineKey, ticketNumber)
              redis.zrem(queryKey, ticketNumber)
              Some(rank)
            }
            else {
              redis.zadd(queryKey, timestamp, ticketNumber)
              Some(rank)
            }
          }
      }
    }
  }

  override def getCurrentQueueSize(key: String, queryInterval: FiniteDuration): Long = {
    val timestamp = Calendar.getInstance().getTimeInMillis
    val lineKey = s"${key}_line"
    val queryKey = s"${key}_query"

    clients.withClient {
      redis => {
        redis
          .pipeline { p =>
            p.zremrangebyscore(queryKey, RedisQueueServiceImpl.FIRST, timestamp - queryInterval.toMillis)
            p.zinterstore(lineKey, List(lineKey, queryKey), RedisClient.MIN)
            p.zcard(lineKey)
          }
          .flatMap(_.last.asInstanceOf[Option[Long]])
          .getOrElse(RedisQueueServiceImpl.NO_ELEMENTS)
      }
    }
  }
}

object RedisQueueServiceImpl
{
  private val FIRST = 0
  private val NO_ELEMENTS = 0
}
