package hr.laplacian.laplas.redis

import scala.concurrent.duration.{Duration, FiniteDuration}

trait RedisService
{
  def set(key : String, value : String)                         : Boolean
  def setEx(key : String, value : String, ttl : FiniteDuration) : Boolean
  def get(key : String)                                         : Option[String]

  def del(key : String)                                         : Long
  def expire                                  (key: String, ttl: FiniteDuration) : Boolean
  def getTTL                                  (key: String)                      : Option[Duration]

  def zRem(key : String, member : String)                        : Boolean
  def zCard(key : String)                                        : Long
  def zScore(key : String, member : String)                      : Option[Double]
  def zRevRank(key : String, member : String)                    : Option[Long]
  def zAdd(key : String, score : Double, member : String)        : Long
  def zIncrBy(key : String, score : Double, member : String)     : Double
  def zRank(key : String, member : String)                       : Option[Long]
  def zRange(key: String, start: Long, end: Long)                : List[String]
  def zRevRangeWithScore(key : String, start : Long, end : Long) : List[(String, Double)]

  def zAddByScoreAtomically (key: String, score: Double, member: String) : Long

  def getRateLimitToken(key: String, limit : Long, interval : Duration)        : Option[String]
  def returnRateLimitToken(key : String, token : String)                       : Boolean
  def validateRateLimitToken(key: String, token : String, interval : Duration) : Boolean
  def getCurrentBucketSize(key : String, interval : Duration) : Long
}
