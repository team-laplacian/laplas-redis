val testzVersion   = "0.0.4"
import sbtrelease.ReleasePlugin.autoImport.ReleaseTransformations._

lazy val root = project
  .in(file("."))
  .settings(
    name := "laplas-redis",
    organization := "hr.laplacian.laplas",
    scalaVersion := "2.12.8",
    crossScalaVersions := Seq("2.11.12", "2.12.8"),
    // version := "0.1.6-SNAPSHOT",
    libraryDependencies ++= Seq(
      "net.debasishg" %% "redisclient" % "3.8",
      "javax.inject" % "javax.inject" % "1"
    ),
    publishTo := sonatypePublishTo.value,
    releaseProcess := Seq[ReleaseStep](
      checkSnapshotDependencies,
      inquireVersions,
      runClean,
      runTest,
      setReleaseVersion,
      commitReleaseVersion,
      tagRelease,
      releaseStepCommandAndRemaining("""sonatypeOpen "hr.laplacian.laplas" "Laplas Redis Staging"""".stripMargin),
      releaseStepCommandAndRemaining("+publishSigned"),
      setNextVersion,
      commitNextVersion,
      releaseStepCommandAndRemaining("sonatypeReleaseAll"),
      pushChanges
    )
  )

lazy val `redis-tests` = project
  .in(file("test"))
  .settings(
    name := "redis-tests",
    libraryDependencies ++= Seq(
      "org.scalaz"                 %% "testz-core"    % testzVersion   % "test",
      "org.scalaz"                 %% "testz-stdlib"  % testzVersion   % "test",
      "org.scalaz"                 %% "testz-runner"  % testzVersion   % "test"
    ).map(_.exclude("org.scalaz", "scalaz"))
  ).dependsOn(root)
