
publishMavenStyle := true

licenses := Seq("MIT" -> url("http://opensource.org/licenses/MIT"))

homepage := Some(url("https://gitlab.com/team-laplacian/laplas-redis"))
scmInfo := Some(
  ScmInfo(
    url("https://gitlab.com/team-laplacian/laplas-redis"),
    "scm:git@gitlab.com:team-laplacian/laplas-redis.git"
  )
)
developers := List(
  Developer(id="mfranic", name="Mario Franic", email="mario@laplacian.hr", url=url("https://gitlab.com/mfranic")),
  Developer(id="mkranjac", name="Marko Kranjac", email="marko@laplacian.hr", url=url("https://gitlab.com/mkranjac"))
)
